package pl.sda.xml;

import org.junit.Test;
import pl.sda.xml.model.Car;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CarDiscountGrouperTest {
    @Test
    public void givenCarsWithDiscountAndNotWhenGroupingThenGroupedByDiscount() {
        //given
        Car car1 = Car.builder()
                .id(1L)
                .isDiscounted(false)
                .build();
        Car car2 = Car.builder()
                .id(2L)
                .isDiscounted(true)
                .build();
        Car car3 = Car.builder()
                .id(3L)
                .isDiscounted(false)
                .build();
        List<Car> carsToTest = Arrays.asList(car1, car2, car3);
        //when
        CarDiscountGrouper carDiscountGrouper = new CarDiscountGrouper(carsToTest);
        Map<Boolean, List<Car>> carsGroupedByDiscount = carDiscountGrouper.groupByDiscount();
        //then
        List<Car> discountedCars = carsGroupedByDiscount.get(true);
        List<Car> notDiscountedCars = carsGroupedByDiscount.get(false);
        assertThat(discountedCars)
                .extracting(e -> e.getId())
                .containsExactlyInAnyOrder(2L);
        assertThat(notDiscountedCars)
                .extracting(e -> e.getId())
                .containsExactlyInAnyOrder(1L, 3L);
    }
}