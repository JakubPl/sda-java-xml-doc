package pl.sda.xml;

import pl.sda.xml.model.Car;

import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        //1. Pogrupuj auta w pliku wedlug waluty i zwroc (zaimplementuj to w klasie CarCurrencyGrouper), pamietaj o testach
        //2. Pogrupuj auta wedlug tego czy cena jest po rabacie (zaimplementuj od poczatku w swojej klasie), pamietaj o testach
        //3. Odfiltruj auta, ktore sa po wypadku - element remarks, crashed. pamietaj o testach
        //4. Odfiltruj samochody po znizce, atrybut isAfterDiscount
        //5. utworz nowy projekt
        //6. przerzuc do niego plik menu.xml
        //7. sparsuj plik za pomoca nowo poznanego sposobu i wypisz zawartosc. przetestuj parsowanie

        CarReader carReader = new CarReader();
        final List<Car> cars = carReader.read();
        System.out.println("ID  | Price");
        cars.forEach(e -> System.out.printf("%d %s%n", e.getId(), e.getPrice()));

        CarCurrencyGrouper carGrouper = new CarCurrencyGrouper(cars);
        Map<String, List<Car>> stringListMap = carGrouper.groupByCurrency();

        CarDiscountGrouper carDiscountGrouper = new CarDiscountGrouper(cars);
        Map<Boolean, List<Car>> booleanListMap = carDiscountGrouper.groupByDiscount();


        for (Map.Entry<String, List<Car>> carCurrencyEntry : stringListMap.entrySet()) {
            System.out.println(carCurrencyEntry.getKey());
            for (Car car : carCurrencyEntry.getValue()) {
                System.out.println(car.getId());
            }
            System.out.println("----------");

        }
    }
}
