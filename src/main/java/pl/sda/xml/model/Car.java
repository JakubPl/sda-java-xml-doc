package pl.sda.xml.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
//TODO: lombok
//*postaraj sie, aby ta klasa byla immutable
@Getter
@Builder
@AllArgsConstructor
public class Car {
    private final Long id;
    private final BigDecimal price;
    private final String currency;
    private final boolean isDiscounted;
}
