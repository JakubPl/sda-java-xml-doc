package pl.sda.xml;

import pl.sda.xml.model.Car;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

//make me immutable
public class CarCurrencyGrouper {
    private final List<Car> cars;


    public CarCurrencyGrouper(List<Car> cars) {
        //make me immutable
        this.cars = cars;
    }

    public List<Car> getCars() {
        //make me immutable
        return cars;
    }

    public Map<String, List<Car>> groupByCurrency() {
        //kluczem tutaj jest waluta, a wartoscia lista samochodow
        return cars.stream()
                .collect(Collectors.groupingBy(car -> car.getCurrency()));
    }
}
