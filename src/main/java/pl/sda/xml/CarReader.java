package pl.sda.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pl.sda.xml.model.Car;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CarReader {
    public List<Car> read() {
        final List<Car> cars = new ArrayList<>();
        try {
            //zaadaptowane z https://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
            //otwiera input stream z pliku cars.xml
            InputStream carsInputStream = this.getClass().getClassLoader().getResourceAsStream("cars.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.parse(carsInputStream);

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nodesOfOffer = doc.getElementsByTagName("car");

            System.out.println("----------------------------");

            for (int i = 0; i < nodesOfOffer.getLength(); i++) {

                Node carNode = nodesOfOffer.item(i);

                System.out.println("\nCurrent Element :" + carNode.getNodeName());

                if (carNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element carElement = (Element) carNode;
                    Element discountElement = (Element) carElement.getElementsByTagName("price").item(0);
                    String discountString = discountElement.getAttribute("isAfterDiscount");

                    final String carIdString = carElement.getAttribute("id");
                    final String carPriceString = carElement.getElementsByTagName("price").item(0).getTextContent();
                    final String currency = carElement.getElementsByTagName("currency").item(0).getTextContent();

                    final Long carId = Long.valueOf(carIdString);
                    final BigDecimal price = new BigDecimal(carPriceString);
                    boolean isDiscounted = Boolean.parseBoolean(discountString);

                    System.out.println("Car id : " + carIdString);
                    System.out.println("Salary : " + carPriceString);
                    System.out.println("Currency : " + currency);
                    cars.add(new Car(carId, price, currency, isDiscounted));
                }
            }
        } catch (ParserConfigurationException e) {
            System.out.println("Nieprawidlowa konfiguracja" + e.getMessage());
        } catch (SAXException e) {
            System.out.println("Blad parsowania" + e.getMessage());
        } catch (IOException e) {
            System.out.println("Blad odczytu" + e.getMessage());
        } finally {
            System.out.println("Parsing finished!!\n");
        }
        return cars;
    }
}
