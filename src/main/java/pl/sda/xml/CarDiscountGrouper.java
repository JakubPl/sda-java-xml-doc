package pl.sda.xml;

import pl.sda.xml.model.Car;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//make me immutable
public class CarDiscountGrouper {
    private final List<Car> cars;


    public CarDiscountGrouper(List<Car> cars) {
        //make me immutable
        this.cars = cars;
    }

    public List<Car> getCars() {
        //make me immutable
        return cars;
    }

    public Map<Boolean, List<Car>> groupByDiscount() {
        //kluczem tutaj jest waluta, a wartoscia lista samochodow
        return this.cars.stream()
                .collect(Collectors.groupingBy(e -> e.isDiscounted()));
    }
}
