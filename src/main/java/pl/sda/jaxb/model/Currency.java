package pl.sda.jaxb.model;

public enum Currency {
    PLN, EUR, USD
}
