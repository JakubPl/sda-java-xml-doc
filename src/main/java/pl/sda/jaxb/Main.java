package pl.sda.jaxb;

import pl.sda.jaxb.model.Car;
import pl.sda.jaxb.model.Offer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) throws JAXBException {
        InputStream carsInputStream = Main.class.getClassLoader().getResourceAsStream("cars.xml");

        JAXBContext jaxbContext = JAXBContext.newInstance(Offer.class);
        Unmarshaller offerUnmarshaller = jaxbContext.createUnmarshaller();
        Offer offer = (Offer) offerUnmarshaller.unmarshal(carsInputStream);

        Car car = new Car();
        car.setColor("orange");
        offer.getCars().add(car);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(offer, new File("result.xml"));

    }
}
